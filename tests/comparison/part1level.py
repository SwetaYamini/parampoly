import parampoly as pp
import islpy as isl

print("Creating ISL set using")
print("""
[n] -> {[in, out] : in >= 0 and out >= 0 and
                    in < 17 and 17*out + in < n}
""")
ibset = isl.BasicSet("""
                    [n] -> {[in, out] :
                        in >= 0 and
                        out >= 0 and
                        in < 17 and
                        17*out + in < n}
                    """)
print("ISL set:")
print(ibset)

print()
print("Creating Parampoly set using")
print("""
[n, b] -> {[in, out] : in >= 0 and out >= 0 and
                       in < b and b*out + in < n}
""")
pbset = pp.BasicSet("""
                    [n, b] -> {[in, out] :
                        in >= 0 and
                        out >= 0 and
                        in < b and
                        b*out + in < n}
                    """)
print("Parampoly set:")
print(pbset)
print()
pbset.add_assumption("b > 0")

print("")

print("Variable Elimination:")
print("Eliminating 'in':")
print("ISL:")
ielin = ibset.eliminate_dims(0, 1)  # pylint: disable=no-member
print(ielin)
print("Parampoly:")
pelin = pbset.eliminate('in')
print(pelin)
print()

print("Eliminating 'out':")
print("ISL:")
ielout = ibset.eliminate_dims(1, 1)  # pylint: disable=no-member
print(ielout)
print("Parampoly:")
pelout = pbset.eliminate('out')
print(pelout)
print()
