import pymbolic as pmbl
from enum import Enum


def convert_if_needed(expr):
    if isinstance(expr, str):
        return pmbl.parse(expr)
    return expr


class SignType(Enum):
    Zero = 0
    Positive = 1
    Negative = 2
    Indeterminate = 3
