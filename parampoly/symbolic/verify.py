import pymbolic as pmbl
from parampoly.symbolic import SignType


class SignMapper(pmbl.mapper.Mapper):
    def __init__(self, context=None):
        if context:
            self.context = context
        else:
            from parampoly.symbolic.context import Context
            self.context = Context.empty()

    def map_constant(self, expr):
        if expr == 0:
            return SignType.Zero
        elif expr > 0:
            return SignType.Positive

        else:
            return SignType.Negative

    def map_product(self, expr):
        signs = [self.rec(child) for child in expr.children]
        if SignType.Zero in signs:
            return SignType.Zero

        elif SignType.Indeterminate in signs:
            return SignType.Indeterminate

        neg = signs.count(SignType.Negative)
        if neg % 2 == 0:
            return SignType.Positive
        else:
            return SignType.Negative

    def map_variable(self, expr):
        return self.context.get_sign(expr)


def get_sign(expr, context=None):
    sm = SignMapper(context)
    return sm(expr)


if __name__ == "__main__":
    # constants
    print(get_sign(4))
    print(get_sign(-100))
    print(get_sign(0))
