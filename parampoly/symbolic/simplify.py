import pymbolic as pmbl


# A bag of mappers that perform some arithmetic simplifications


def simplify(expr):
    expr = pmbl.mapper.constant_folder.CommutativeConstantFoldingMapper()(expr)
    expr = pmbl.mapper.collector.TermCollector()(expr)
    expr = pmbl.mapper.distributor.DistributeMapper()(expr)
    return expr


if __name__ == "__main__":
    from parampoly.symbolic import convert_if_needed

    expr = convert_if_needed("b + (-1)*b")
    print(expr)
    print(simplify(expr))
