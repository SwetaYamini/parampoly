import pymbolic as pmbl
from parampoly.symbolic import SignType


class Context(object):
    def __init__(self, params=[]):
        self.params = params
        self.assumptions = []

    @staticmethod
    def empty():
        return Context()

    def add_assumption(self, cnstr):
        from parampoly.symbolic import convert_if_needed
        expr = convert_if_needed(cnstr)
        from parampoly.symbolic.mappers import VariableMapper
        variables = VariableMapper()(expr)
        for v in variables:
            assert v in self.params, ("Parameter " + v +
                                      " not in space " + str(self.params))
        self.assumptions.append(expr)

    def verify_and_add(self, cnstr):
        self.verify(cnstr)
        self.add_assumption(cnstr)

    def verify(self, expr):
        pass

    def get_sign(self, expr):
        if pmbl.primitives.Comparison(expr, "==", 0) in self.assumptions:
            return SignType.Zero
        elif pmbl.primitives.Comparison(expr, ">", 0) in self.assumptions:
            return SignType.Positive
        elif pmbl.primitives.Comparison(expr, "<", 0) in self.assumptions:
            return SignType.Negative
        else:
            return SignType.Indeterminate
