import pymbolic as pmbl


class VariableMapper(pmbl.mapper.Collector):
    def map_variable(self, expr):
        return {expr.name}


if __name__ == "__main__":
    # test VariableMapper
    expr = pmbl.parse("a1+b1*a+d/e")
    vm = VariableMapper()
    print(vm(expr))
    expr2 = pmbl.parse("a+a")
    print(VariableMapper()(expr2))
