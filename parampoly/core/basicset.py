from parampoly.core import SetBase
from parampoly.core.space import Space
from parampoly.core.constraint import Constraint

from parampoly.symbolic.context import Context
from parampoly.symbolic.simplify import simplify


class BasicSet(SetBase):
    def __init__(self, space, constraints=[]):
        if isinstance(space, str):
            self.parse_from_str(space)
        else:
            self.space = space
            self.constraints = constraints
            for c in constraints:
                assert c.space == space, ("Mismatched constraint " +
                                          str(c) + " in space " + str(space))
        self.context = Context(self.space.params)

    def parse_from_str(self, string):
        from parampoly.support.parser import parse_basic_set
        results = parse_basic_set(string)
        params = results.get("params", [])
        vars = results.get("vars", [])
        self.space = Space(params, vars)
        self.constraints = []
        if "constraints" in results:
            for cnstr in results["constraints"]:
                self.constraints.append(Constraint.from_expression(
                                        self.space, cnstr))

    def __str__(self):
        parts = []
        parts.append(str(self.space.params))
        parts.append("->")
        parts.append("{")
        parts.append(str(self.space.vars))
        parts.append(":")
        j = ""
        for c in self.constraints:
            parts.append(j)
            j = "and"
            parts.append(str(c))
        parts.append("}")
        return " ".join(parts)

    def set_context(self, context):
        assert(context.params == self.space.params)
        self.context = context

    def add_constraint(self, cnstr):
        if isinstance(cnstr, Constraint):
            assert cnstr.space == self.space, ("Mismatched constraint " +
                                               str(cnstr) + " in space " +
                                               str(self.space))
            self.constraints.append(cnstr)
        else:
            self.constraints.append(Constraint.from_expression(self.space,
                                                               cnstr))

    def add_assumption(self, assm):
        self.context.add_assumption(assm)

    def eliminate(self, name):
        cnstr = [c for c in self.constraints if c.is_non_zero(name)]
        from parampoly.symbolic import SignType
        undefined = [c for c in cnstr
                     if c.get_sign(name, self.context) ==
                     SignType.Indeterminate]
        if undefined:
            print("Unable to determine sign of " + name +
                  " in constraint " + str(undefined[0]))
            return None
        min_cnstr = [c for c in cnstr
                     if c.get_sign(name, self.context) ==
                     SignType.Positive]
        max_cnstr = [c for c in cnstr
                     if c.get_sign(name, self.context) ==
                     SignType.Negative]

        context = Context(self.space.params)
        new_constraints = []
        for minc in min_cnstr:
            for maxc in max_cnstr:
                coeffmin = minc.get_coeff(name)
                coeffmax = maxc.get_coeff(name)
                ncoeffmax = simplify(-1*coeffmax)
                nminc = minc
                nmaxc = maxc
                if(ncoeffmax != coeffmin):
                    if ncoeffmax != 1:
                        nminc = Constraint.Multiply(minc, ncoeffmax)
                    if coeffmin != 1:
                        nmaxc = Constraint.Multiply(maxc, coeffmin)
                newconstraint = Constraint.Add(nminc, nmaxc)
                variables = newconstraint.get_nonzero_variables()
                if variables:
                    new_constraints.append(newconstraint)
                else:
                    context.verify_and_add(newconstraint.get_expr())

        cnstr = [c for c in self.constraints if not c.is_non_zero(name)]
        cnstr.extend(new_constraints)
        bset = BasicSet(self.space, cnstr)
        bset.set_context(context)
        return bset

    def move_dim_to_param(self, name):
        assert name in self.space.vars, ("Variable " + name +
                                         " not found in dims " +
                                         str(self.space.vars))
        space = Space([n for n in self.space.params] + [name],
                      [n for n in self.space.vars if n != name])
        cnstrs = [c.move_dim_to_param(space, name) for c in self.constraints]
        bset = BasicSet(space, [c for c in cnstrs
                                if c.get_nonzero_variables()])
        for c in cnstrs:
            if not c.get_nonzero_variables():
                bset.add_assumption(c.get_expr())
        for a in self.context.assumptions:
            bset.add_assumption(a)
        return bset

    def eliminate_except(self, names):
        eliminate = [n for n in self.space.vars if n not in names]
        bset = self
        for elem in eliminate:
            bset = bset.eliminate(elem)
            bset.set_context(self.context)
        return bset

    def dim_max(self, name):
        bset = self.eliminate_except([name])
        cnstr = [c for c in bset.constraints if c.is_non_zero(name)]
        from parampoly.symbolic import SignType
        undefined = [c for c in cnstr
                     if c.get_sign(name, bset.context) ==
                     SignType.Indeterminate]
        if undefined:
            print("Unable to determine sign of " + name +
                  " in constraint " + str(undefined[0]))
            return None
        max_vals = [c.get_max_expr(name, bset.context) for c in cnstr
                    if c.get_sign(name, self.context) == SignType.Negative]
        return max_vals

    def dim_min(self, name):
        bset = self.eliminate_except([name])
        cnstr = [c for c in bset.constraints if c.is_non_zero(name)]
        from parampoly.symbolic import SignType
        undefined = [c for c in cnstr
                     if c.get_sign(name, bset.context) ==
                     SignType.Indeterminate]
        if undefined:
            print("Unable to determine sign of " + name +
                  " in constraint " + str(undefined[0]))
            return None
        min_vals = [c.get_min_expr(name, bset.context) for c in cnstr
                    if c.get_sign(name, self.context) == SignType.Positive]
        return min_vals

if __name__ == "__main__":
    def print_dim(exprlist):
        result = "["
        join = ""
        for exprs in exprlist:
            result += join
            join = ", "
            if isinstance(exprs, tuple):
                result += ("integral_div((" + str(exprs[0]) + ")/" +
                           str(exprs[1]) + ")")
            else:
                result += str(exprs)
        result += "]"
        print(result)

    space = Space(["m", "n"], ["i", "j"])
    # i >= 0
    c1 = Constraint(space, {"i": 1})
    # j >= 0
    c2 = Constraint(space, {"j": 1})
    # -i + m -1 >= 0 => i <= m-1
    c3 = Constraint(space, {"i": -1, "m": 1, 1: -1})
    # -j + n -1 >= 0 => j <= n-1
    c4 = Constraint(space, {"j": -1, "n": 1, 1: -1})
    bset = BasicSet(space, [c1, c2, c3, c4])
    print(bset)

    space2 = Space(["n", "b"], ["in", "out"])
    # in >= 0
    c21 = Constraint(space2, {"in": 1})
    # out >= 0
    c22 = Constraint(space2, {"out": 1})
    # -in + b -1 >= 0 => in <= b-1
    c23 = Constraint(space2, {"in": -1, 1: "b-1"})
    # -b*out -in + n-1 >= 0 => b*out + in <= n-1
    c24 = Constraint(space2, {"out": "-b", "in": -1, 1: "n-1"})
    bset2 = BasicSet(space2, [c21, c22, c23, c24])
    print(bset2)

    noin = bset2.eliminate("in")
    print(bset2)
    print(noin)

    noout = bset2.eliminate("out")
    print(bset2)
    print(noout)

    bset2.add_assumption("b > 0")
    noout = bset2.eliminate("out")
    print(bset2)
    print(noout)

    print_dim(bset2.dim_min("in"))
    print_dim(bset2.dim_max("in"))
    print_dim(bset2.dim_min("out"))
    print_dim(bset2.dim_max("out"))

    outparam = bset2.move_dim_to_param("out")
    print_dim(outparam.dim_min("in"))
    print_dim(outparam.dim_max("in"))

    space3 = Space(["n", "b1", "b2"], ["o", "io", "ii"])
    # o >= 0
    c31 = Constraint(space3, {"o": 1})
    # io >= 0
    c32 = Constraint(space3, {"io": 1})
    # ii >= 0
    c33 = Constraint(space3, {"ii": 1})
    # -ii + b2 - 1 >= 0
    c34 = Constraint(space3, {"ii": -1, 1: "b2-1"})
    # -b2*io -ii + b1 - 1 >= 0
    c35 = Constraint(space3, {"io": "-b2", "ii": -1, 1: "b1-1"})
    # -b1*o -b2*io -ii + n-1 >= 0
    c36 = Constraint(space3, {"o": "-b1", "io": "-b2", "ii": -1, 1: "n-1"})
    bset3 = BasicSet(space3, [c31, c32, c33, c34, c35, c36])
    print(bset3)
    bset3.add_assumption("b1 > 0")
    bset3.add_assumption("b2 > 0")

    noii = bset3.eliminate("ii")
    print(noii)

    noio = bset3.eliminate("io")
    print(noio)

    noo = bset3.eliminate("o")
    print(noo)

    oparam = bset3.move_dim_to_param("o")
    print(oparam)

    print_dim(bset3.dim_min("ii"))
    print_dim(bset3.dim_max("ii"))
    print_dim(bset3.dim_min("io"))
    print_dim(bset3.dim_max("io"))
    print_dim(bset3.dim_min("o"))
    print_dim(bset3.dim_max("o"))

    print_dim(oparam.dim_min("io"))
    print_dim(oparam.dim_max("io"))

    onlyii = oparam.move_dim_to_param("io")
    print(onlyii)
    print_dim(onlyii.dim_min("ii"))
    print_dim(onlyii.dim_max("ii"))

    pbset = BasicSet("""
                    [n, b] -> {[in, out] :
                        in >= 0 and
                        out >= 0 and
                        in < b and
                        b*out + in < n}
                    """)
    print(pbset)
