from parampoly.core import ParampolyBase
from parampoly.symbolic import convert_if_needed
from parampoly.symbolic.simplify import simplify
from parampoly.symbolic.integral import convert_to_integer


class Constraint(ParampolyBase):
    def __init__(self, space, coeffs):
        if 1 not in coeffs:
            coeffs[1] = 0
        del_list = []
        for k in coeffs.keys():
            assert(k in space.params or k in space.vars or k == 1)
            # convert to pymbolic
            coeffs[k] = convert_if_needed(coeffs[k])
            # convert params to constant term
            if k in space.params:
                coeffs[1] += coeffs[k]*convert_if_needed(k)
                del_list.append(k)
        coeffs[1] = simplify(coeffs[1])
        for d in del_list:
            del coeffs[d]
        self.space = space
        self.coeffs = coeffs

    @staticmethod
    def from_expression(space, expression):
        expression = convert_if_needed(expression)

        from pymbolic.mapper import Mapper

        class Converter(Mapper):
            def __init__(self, space):
                self.space = space
                self.coeffs = {k: 0 for k in space.vars}
                self.coeffs[1] = 0

            def map_comparison(self, expr):
                if expr.operator == "<":
                    child = simplify(expr.right - 1 - expr.left)
                elif expr.operator == "<=":
                    child = simplify(expr.right - expr.left)
                elif expr.operator == ">":
                    child = simplify(expr.left - 1 - expr.right)
                elif expr.operator == ">=":
                    child = simplify(expr.left - expr.right)
                else:
                    raise NotImplementedError("Operator == " +
                                              "and != not yet supported!")
                self.rec(child)
                return {k: simplify(v) for k, v in self.coeffs.items()}

            def map_sum(self, expr):
                for child in expr.children:
                    self.rec(child)

            def map_product(self, expr):
                coeffs = 1
                base = []
                from pymbolic.primitives import Variable
                from pymbolic.primitives import VALID_CONSTANT_CLASSES
                for child in expr.children:
                    if isinstance(child, VALID_CONSTANT_CLASSES):
                        coeffs *= child
                    elif isinstance(child, Variable):
                        if child.name in self.space.vars:
                            base.append(child)
                        elif child.name in self.space.params:
                            coeffs *= child
                        else:
                            print("ERROR: unknown name " + expr.name)
                    else:
                        from parampoly.symbolic.mappers import VariableMapper
                        vars = VariableMapper()(child)
                        if self.space.vars.isdisjoint(vars):
                            coeffs *= child
                        else:
                            print("ERROR: complex expression in variable " +
                                  str(child))
                if len(base) == 0:
                    self.coeffs[1] += coeffs
                elif len(base) == 1:
                    self.coeffs[base[0].name] += coeffs
                else:
                    print("ERROR: Multiplication of vars is not permitted!")

            def map_constant(self, expr):
                # This is only called from sum
                # Thus, this is a constant term
                self.coeffs[1] += expr

            def map_variable(self, expr):
                # This is only called from sum
                # Thus, this is a singleton element
                if expr.name in self.space.params:
                    self.coeffs[1] += expr
                elif expr.name in self.space.vars:
                    self.coeffs[expr.name] += 1
                else:
                    print("ERROR: unknown name " + expr.name)

        coeffs = Converter(space)(expression)
        return Constraint(space, coeffs)

    def __str__(self):
        parts = []
        terms = []
        for k, v in self.coeffs.items():
            if v != 0:
                if v != 1:
                    if(k != 1):
                        terms.append('(' + str(v) + ')*' + str(k))
                    else:
                        terms.append(str(v))
                else:
                    terms.append(str(k))
        parts.append(" + ".join(terms))
        parts.append(">= 0")
        return " ".join(parts)

    def is_non_zero(self, name):
        if name in self.coeffs and self.coeffs[name] != 0:
            return True
        return False

    def get_sign(self, name, context=None):
        from parampoly.symbolic.verify import get_sign
        return get_sign(self.coeffs[name], context)

    def get_coeff(self, name):
        if name in self.coeffs:
            return self.coeffs[name]
        elif name in self.space.vars:
            return 0
        else:
            raise ValueError("Unknown variable " + name)

    @staticmethod
    def Add(left, right):
        assert(left.space == right.space)
        coeffs = {**left.coeffs, **right.coeffs,
                  **{k: simplify(left.coeffs[k] + right.coeffs[k])
                     for k in left.coeffs.keys() & right.coeffs}}
        return Constraint(left.space, coeffs)

    @staticmethod
    def Multiply(cnstr, expr):
        coeffs = {k: simplify(expr*cnstr.coeffs[k]) for k in cnstr.coeffs}
        return Constraint(cnstr.space, coeffs)

    def get_nonzero_variables(self):
        variables = []
        for k, v in self.coeffs.items():
            if k in self.space.vars and v != 0:
                variables.append(k)
        return variables

    def get_expr(self):
        from pymbolic.primitives import Comparison
        expr = 0
        for k, v in self.coeffs.items():
            expr += v*convert_if_needed(k)
        return simplify(Comparison(expr, ">=", 0))

    def move_dim_to_param(self, space, name):
        coeffs = {k: self.coeffs[k] for k in self.coeffs if k != name}
        if name in self.coeffs:
            coeffs[1] = simplify(self.coeffs[1] +
                                 self.coeffs[name]*convert_if_needed(name))
        return Constraint(space, coeffs)

    def get_min_expr(self, name, context):
        rest = 0
        for k, v in self.coeffs.items():
            if k != name:
                rest += v*convert_if_needed(k)
        return convert_to_integer((simplify(-1*rest), self.coeffs[name]),
                                  context)

    def get_max_expr(self, name, context):
        rest = 0
        for k, v in self.coeffs.items():
            if k != name:
                rest += v*convert_if_needed(k)
        return convert_to_integer((simplify(rest),
                                   simplify(-1*self.coeffs[name])),
                                  context)
