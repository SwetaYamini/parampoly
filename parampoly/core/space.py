from parampoly.core import ParampolyBase


class VariableSet(set):
    def __str__(self):
        parts = []
        parts.append('[')
        parts.append(', '.join(str(vars) for vars in self))
        parts.append(']')
        return ''.join(parts)


class Space(ParampolyBase):
    def __init__(self, params=[], vars=[]):
        self.params = VariableSet(params)
        self.vars = VariableSet(vars)

    def __str__(self):
        parts = []
        parts.append(str(self.params))
        parts.append("->")
        parts.append("{")
        parts.append(str(self.vars))
        parts.append("}")
        return " ".join(parts)


if __name__ == "__main__":
    space = Space(["n"], ["i", "j"])
    print(space)
