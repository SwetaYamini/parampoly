from parampoly.core import SetBase


class Context(SetBase):
    def __init__(self, space, constraints):
        assert(space.params == [])
        self.space = space
        self.constraints = constraints
