from lark import Lark, Transformer


class BasicSetTransformer(Transformer):
    def id(self, s):
        return str(s[0])

    def int(self, n):
        return int(n[0])

    def plus(self, n):
        return "plus"

    def minus(self, n):
        return "minus"

    def le(self, n):
        return "<="

    def ge(self, n):
        return ">="

    def lt(self, n):
        return "<"

    def gt(self, n):
        return ">"

    def eq(self, n):
        return "=="

    def id_factor(self, n):
        from pymbolic.primitives import Variable
        return Variable(n[0])

    def term(self, n):
        val = 1
        from parampoly.symbolic.simplify import simplify
        for t in n:
            val *= t
        return simplify(val)

    def expression(self, n):
        val = 0
        from parampoly.symbolic.simplify import simplify
        factor = 1
        for t in n:
            if t == 'plus':
                factor = 1
            elif t == 'minus':
                factor = -1
            else:
                val += factor*t
        return simplify(val)

    def constraint(self, n):
        from pymbolic.primitives import Comparison
        return Comparison(n[0], n[1], n[2])

    def start(self, n):
        result = dict()
        for t in n:
            if t.data == "params":
                result["params"] = t.children
            if t.data == "vars":
                result["vars"] = t.children
            if t.data == "constraints":
                result["constraints"] = t.children
        return result


def parse_basic_set(string):
    parser = Lark.open('parampoly/support/parse.lark',
                       parser='lalr',
                       transformer=BasicSetTransformer())
    return parser.parse(string)


if __name__ == "__main__":
    example = """[n, b] -> {[in, out] :
    0 <= in and 0 <= out and in < b and b out + in < n}"""
    print(parse_basic_set(example))
